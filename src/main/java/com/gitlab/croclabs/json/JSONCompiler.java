package com.gitlab.croclabs.json;

@SuppressWarnings("all")
public interface JSONCompiler<T, E> {
	public JSONCompiler<T, E> enable(E... feature);
	public JSONCompiler<T, E> disable(E... feature);
}
