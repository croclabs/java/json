package com.gitlab.croclabs.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;
import java.util.logging.Logger;

public class JSONParser<T, E> implements JSONCompiler<T, Feature> {
	private static final Logger LOG = Logger.getLogger(JSONParser.class.getName());
	protected JsonFactory factory = new JsonFactory();
	protected T json;
	protected Class<E> type;

	@SuppressWarnings("unchecked")
	protected JSONParser(T json) {
		factory.enable(Feature.IGNORE_UNDEFINED)
				.enable(Feature.ALLOW_COMMENTS)
				.enable(Feature.ALLOW_SINGLE_QUOTES)
				.enable(Feature.ALLOW_UNQUOTED_FIELD_NAMES);

		this.json = json;
		this.type = (Class<E>) JsonNode.class;
	}

	protected JSONParser(T json, Class<E> clazz) {
		factory.enable(Feature.IGNORE_UNDEFINED)
				.enable(Feature.ALLOW_COMMENTS)
				.enable(Feature.ALLOW_SINGLE_QUOTES)
				.enable(Feature.ALLOW_UNQUOTED_FIELD_NAMES);

		this.json = json;
		this.type = clazz;
	}

	public JSONParser<T, E> enable(Feature... feature) {
		Arrays.stream(feature).forEach(f -> factory.enable(f));
		return this;
	}

	public JSONParser<T, E> disable(Feature... feature) {
		Arrays.stream(feature).forEach(f -> factory.disable(f));
		return this;
	}

	@SuppressWarnings("unchecked")
	public <P> JSONParser<P, E> json(P json) {
		this.json = (T) json;
		return (JSONParser<P, E>) this;
	}

	@SuppressWarnings("unchecked")
	public <P> JSONParser<T, P> target(Class<P> clazz) {
		type = (Class<E>) clazz;
		return (JSONParser<T, P>) this;
	}

	@SuppressWarnings("unchecked")
	public <P> JSONParser<T, P> target(P target) {
		type = (Class<E>) target.getClass();
		return (JSONParser<T, P>) this;
	}

	public E parse() {
		return parse(type);
	}

	protected E parse(Class<E> type) {
		Objects.requireNonNull(
				json,
				"No JSON found to parse, make sure to set it via .json() or in JSON.parser()!"
		);

		JsonMapper mapper = new JsonMapper(factory);

		try {
			if (json instanceof String) {
				return mapper.readValue((String) json, type);
			} else if (json instanceof URL) {
				return mapper.readValue((URL) json, type);
			} else if (json instanceof File) {
				return mapper.readValue((File) json, type);
			} else if (json instanceof Reader) {
				return mapper.readValue((Reader) json, type);
			} else if (json instanceof byte[]) {
				return mapper.readValue((byte[]) json, type);
			} else if (json instanceof InputStream) {
				return mapper.readValue((InputStream) json, type);
			} else {
				LOG.warning("Unsupported json type");
				return null;
			}
		} catch (IOException e) {
			LOG.severe("Reading JSON value failed");
			e.printStackTrace();
			return null;
		}
	}
}
