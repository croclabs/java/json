package com.gitlab.croclabs.json;

import com.fasterxml.jackson.databind.JsonNode;

// TODO add case for type ref
public class JSON {
	protected JSON() {
		super();
	}

	public static JSONParser<Object, JsonNode> parser() {
		return new JSONParser<>(null, JsonNode.class);
	}

	public static <T> JSONParser<T, JsonNode> parser(T json) {
		return new JSONParser<>(json, JsonNode.class);
	}

	public static JSONComposer<Object> composer() {
		return new JSONComposer<>(null);
	}

	public static <T> JSONComposer<T> composer(T object) {
		return new JSONComposer<>(object);
	}
}
