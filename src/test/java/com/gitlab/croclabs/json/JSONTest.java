package com.gitlab.croclabs.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class JSONTest {
	public static class TestJson {
		int test;
		String test2;

		public int getTest() {
			return test;
		}

		public void setTest(int test) {
			this.test = test;
		}

		public String getTest2() {
			return test2;
		}

		public void setTest2(String test2) {
			this.test2 = test2;
		}
	}

	@Test
	protected void json() throws IOException {
		JSON jsonUtil = new JSON();

		JsonNode node = JSON.parser("{'test': 1, 'test2': 'man'}")
				.disable(Feature.ALLOW_COMMENTS)
				.enable(Feature.ALLOW_COMMENTS)
				.target(JsonNode.class)
				.parse();

		JsonNode node2 = JSON.parser().json("{'test': 1, 'test2': 'man'}").parse();

		JsonNode node3 = JSON.parser("{'test': 1, 'test2': 'man'}".getBytes(StandardCharsets.UTF_8)).parse();

		TestJson testJson = JSON.parser("{'test': 1, 'test2': 'man'}".getBytes(StandardCharsets.UTF_8))
				.target(TestJson.class)
				.parse();

		Map<String, Object> map = JSON.parser("{'test': 1, 'test2': 'man'}")
				.target(new HashMap<String, Object>())
				.parse();

		List<String> list = JSON.parser("['test1', 'test2']")
				.target(new ArrayList<String>())
				.parse();

		assertNotNull(node);
		assertEquals(1, node.get("test").asInt());
		assertEquals("man", node.get("test2").asText());

		assertNotNull(node);
		assertEquals(1, node2.get("test").asInt());
		assertEquals("man", node2.get("test2").asText());

		assertNotNull(node);
		assertEquals(1, node3.get("test").asInt());
		assertEquals("man", node3.get("test2").asText());

		assertNotNull(testJson);
		assertEquals(1, testJson.getTest());
		assertEquals("man", testJson.getTest2());

		assertNotNull(map);
		assertEquals(1, map.get("test"));
		assertEquals("man", map.get("test2"));

		assertNotNull(list);
		assertEquals("test1", list.get(0));
		assertEquals("test2", list.get(1));

		String json = JSON.composer(node).disable(JsonGenerator.Feature.IGNORE_UNKNOWN)
				.enable(JsonGenerator.Feature.IGNORE_UNKNOWN).compose();

		String json2 = JSON.composer().object(testJson).compose();
		String jsonPretty = JSON.composer(testJson).pretty().compose();

		assertEquals("{\"test\":1,\"test2\":\"man\"}", json);
		assertEquals("{\"test\":1,\"test2\":\"man\"}", json2);
		assertEquals("{" + System.lineSeparator() + "  \"test\" : 1," + System.lineSeparator() +
				"  \"test2\" : \"man\"" + System.lineSeparator() + "}", jsonPretty);

		byte[] jsonBytes = JSON.composer(node).composeBytes();

		assertNotEquals(0, jsonBytes.length);

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			JSON.composer(node).compose(out);
			assertNotEquals(0, out.toByteArray().length);
		}
	}

}